import React from 'react';
import 'normalize.css';
import './App.css';
import Layout from './components/Layout.js'
import ProductList from './features/products/ProductList.js'
import Cart from './features/cart/Cart.js'
import { Switch,Route } from "react-router-dom";

function App() {
  return (
    <Layout>
      <Switch>
        <Route exact path="/"><ProductList /></Route>
        <Route path="/cart"><Cart /></Route>
      </Switch>
    </Layout>
  );
}

export default App;
