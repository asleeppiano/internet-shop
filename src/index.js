import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './app/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import AppContext from './context'
import { setCart } from './features/cart/cartSlice.js'

export function init(dealers) {
  const cart = JSON.parse(localStorage.getItem('cart'))
  if(cart) {
    store.dispatch(setCart(cart))
  }
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <AppContext.Provider value={dealers}>
          <App />
        </AppContext.Provider>
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );
}

window.init = init

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
