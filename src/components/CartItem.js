/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import { HOST_ADDR } from '../constants'
import { useDispatch } from 'react-redux'
import { incrementItemCount, decrementItemCount } from '../features/cart/cartSlice.js'

import Button from './Button.js'
import CardSurface from './CardSurface.js'

const flex = css`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  position: relative;
  &:hover .close-btn {
    visibility: visible;
  }
`

const left = css`
  display: flex;
  align-items: center;
`

const h2 = css`
  margin-top: 0;
  margin-bottom:0;
  margin-left: 0.5rem;
`

const img = css`
  max-width: 60px;
  max-height: 60px;
  width: 60px;
  height: 60px;
`

const count = css`
  margin: 0 0.5rem;
`

const closeButton = css`
  position: absolute;
  right: 0;
  top: 0;
  visibility: hidden;
  background: transparent;
  border: none;
  cursor: pointer;
  font-size: 1.2rem;
  @media (hover: none), (pointer: coarse) {
    visibility: visible;
  }
  &:hover {
    background: var(--light-color);
  }
`

const right = css`
  display: flex;
  flex-direction: column;
  @media (min-width: 420px) {
    flex-direction: row;
    align-items: center;
  }
`

const priceBlock = css`
  margin-bottom: 1rem;
  text-align: right;
  @media (min-width: 420px) {
    margin-bottom: 0rem;
    margin-right: 1rem;
  }
`

const priceCount = css`
  font-size: 0.875rem;
  color: var(--gray-500);
`

export default function CartItem(props) {
  const { product } = props
  const dispatch = useDispatch()

  function removeItem() {
    props.onRemoveItem(product)
  }

  function decrement() {
    if(product.count === 1) {
      props.onRemoveItem(product)
      return
    }
    dispatch(decrementItemCount(product))
  }

  function increment() {
    dispatch(incrementItemCount(product))
  }

  return (
    <CardSurface style={flex}>
      <div css={left}>
        <img css={img} src={`${HOST_ADDR}${product.item.image}`} alt={product.name} />
        <h2 css={h2}>{product.item.name}</h2>
      </div>
      <div css={right}>
        <div css={priceBlock}>
          <div>${(product.item.price * product.count).toFixed(2)}</div>
          {product.count > 1 ? <div css={priceCount}>{product.count} x ${product.item.price}</div> : ''}
        </div>
        <div>
          <Button type="button" onClick={decrement}>-</Button>
          <span css={count}>{product.count}</span>
          <Button type="button" onClick={increment}>+</Button>
        </div>
      </div>
      <button className="close-btn" css={closeButton} onClick={removeItem}>&#10005;</button>
    </CardSurface>
  )
};
