/** @jsx jsx */
import { useSelector } from 'react-redux';
import { NavLink, Link } from "react-router-dom";
import { jsx, css } from '@emotion/core'
import { selectCartSize } from '../features/cart/cartSlice.js'

import { container } from './Container.js'

const header = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem;
  position: sticky;
  top: 0;
`

const title = css`
  font-size: 1.25rem;
  font-weight: 700;
  text-decoration: none;
  background: white;
  padding: 0.2rem 0.5rem;
  &:visited {
    color: black;
  }
`

const ul = css`
  display: flex;
  padding: 0;
  margin:0;
  background: white;
  padding: 0.2rem 0.5rem;
`

const li = css`
  list-style-type: none;
  &:not(:first-of-type) {
    margin-left: 1rem;
  }
`

const link = css`
  color: var(--main-color);
  &:visited {
    color: var(--main-color);
  }
`

const cartSizeStyle = css`
  margin-left: 0.3rem;
  color: var(--main-color);
  display: inline-block;
`

const activeLinkStyle = {
  color: 'var(--extra-color)'
}

export default function Header() {
  const cartSize = useSelector(selectCartSize)
  return (
    <header css={[container, header]}>
      <div ><Link css={title} exact="true" to="/">Internet-Shop</Link></div>
      <nav>
        <ul css={ul}>
          <li css={li}><NavLink css={link} activeStyle={activeLinkStyle} exact to="/">list</NavLink></li>
          <li css={li}><NavLink css={link} activeStyle={activeLinkStyle} to="/cart">cart</NavLink>{cartSize ? <span css={cartSizeStyle}>{cartSize}</span> : ''}</li>
        </ul>
      </nav>
    </header>
  )
}
