/** @jsx jsx */
import { jsx, css } from '@emotion/core'

export const container = css`
  width: 100%;
  padding: 1rem 1rem 2rem 1rem;
  margin: 0 auto;
  @media (min-width: 640px) {
    max-width: 640px;
  }
  @media (min-width: 768px) {
    max-width: 768px;
  }
  @media (min-width: 1024px) {
    max-width: 1024px;
  }
  @media (min-width: 1240px) {
    max-width: 1240px;
  }
`

export default function Container(props) {
  return (
    <div css={container}>
      {props.children}
    </div>
  )
}
