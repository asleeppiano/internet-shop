import React from 'react'
import Header from './Header.js'
import {
  BrowserRouter as Router,
} from "react-router-dom";

export default function Layout({children}) {
  return (
    <Router>
      <Header />
      <main>
        {children}
      </main>
    </Router>
  )
}
