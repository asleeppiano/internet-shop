/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const button = css`
  padding: 0.4rem 0.7rem;
  background: var(--main-color);
  color: var(--light-color);
  border: none;
  border-radius: 6px;
  cursor: pointer;
  box-shadow: 0px 4px 6px -1px rgba(0,0,0,0.2);
  &:hover {
    background-color: var(--sec-color);
    color: var(--main-color);
  }
`

export default function Button(props) {
  return (
    <button onClick={props.onClick} css={[button, props.style]} type={props.type}>
      {props.children}
    </button>
  )
}
