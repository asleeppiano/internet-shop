import ReactDOM from 'react-dom'
import { nanoid } from 'nanoid'

let id = nanoid()
let portalRoot = document.getElementById(id)
export default function Portal(props) {
  if(!portalRoot) {
    portalRoot = document.createElement('div')
    portalRoot.id = id
    document.body.append(portalRoot)
  }

  return (
    ReactDOM.createPortal(props.children, portalRoot)
  )
}
