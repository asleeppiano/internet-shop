/** @jsx jsx */
import { jsx, css } from '@emotion/core'

import Portal from './Portal.js'
import Button from './Button.js'

const overlap = css`
  position: fixed;
  background: hsla(0, 0%, 0%, 0.2);
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 1px solid black;
`

const dialog = css`
  border-radius: 6px;
  background: var(--light-color);
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
  max-width: 400px;
  height: auto;
  padding: 1rem;
`

const flex = css`
  display: flex;
  justify-content: space-between;
`

const okButton = css`
  background: var(--sec-color);
  color: var(--main-color);
  &:hover {
    background: var(--accent-color);
    color: var(--light-color);
  }
  box-shadow: none;
`

const text = css`
  margin-bottom: 1rem;
`

export default function ConfirmDialog(props) {
  if(!props.show) {
    return null
  }
  return (
    <Portal>
      <div onClick={props.onClose} css={overlap}>
        <div onClick={(e) => e.stopPropagation()} css={dialog}>
          <div css={text}>
            {props.text}
          </div>
          <div css={flex}>
            <Button style={okButton} onClick={props.onOk}>ok</Button>
            <Button onClick={props.onClose}>cancel</Button>
          </div>
        </div>
      </div>
    </Portal>
  )
}
