/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const card = css`
  box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06), 0 -1px 2px -1px rgba(0,0,0,0.1);
  padding: 1.5rem 1rem 1rem 1rem;
  border-radius: 6px;
  max-width: 500px;
  margin: 0 auto;
  @media (min-width: 420px) {
    padding: 1rem;
  }
`

export default function CardSurface(props) {
  return (
    <div css={[card, props.style]}>
      {props.children}
    </div>
  )
}
