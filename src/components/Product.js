/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import { useDispatch, useSelector } from 'react-redux'
import { HOST_ADDR } from '../constants'
import { addItem, selectCartItemById, incrementItemCount, selectCartItems } from '../features/cart/cartSlice.js'

import Button from './Button.js'
import CardSurface from './CardSurface.js'

const h2 = css`
  margin-top: 0;
  margin-bottom:0;
`

const img = css`
  max-width: 60px;
  max-height: 60px;
  width: 60px;
  height: 60px;
`

const bottom = css`
  margin-top: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const marginLeft = css`
  margin-left: 1rem;
`
export default function Product(props) {
  const { product } = props
  const cartItem = useSelector(selectCartItemById(product.id))
  const items = useSelector(selectCartItems)
  const dispatch = useDispatch()

  function addToCart() {
    console.log('addToCart one item', cartItem)
    console.log('addToCart items', items)
    if(cartItem) {
      dispatch(incrementItemCount(product))
    } else {
      const newproduct = Object.assign({}, product)
      newproduct.count = 1
      dispatch(addItem(newproduct))
    }
  }

  return (
    <CardSurface>
      <img css={img} src={`${HOST_ADDR}${product.item.image}`} alt={product.item.name} />
      <div css={bottom}>
        <h2 css={h2}>{product.item.name}</h2>
        <div>
          <span>${product.item.price}</span>
          <Button style={marginLeft} type="button" onClick={addToCart}>add to cart</Button>
        </div>
      </div>
    </CardSurface>
  )
};
