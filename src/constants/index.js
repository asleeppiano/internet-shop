export const API_ADDR = 'https://murmuring-tor-81614.herokuapp.com/api'

export const HOST_ADDR = 'https://murmuring-tor-81614.herokuapp.com'

export const FETCH_STATE = {
  FULFILLED: 'FULFILLED',
  LOADING: 'LOADING',
  ERROR: 'ERROR'
}
