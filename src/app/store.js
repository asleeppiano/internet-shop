import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "../features/cart/cartSlice.js";
import productsReducer from "../features/products/productSlice.js";

export default configureStore({
  reducer: {
    cart: cartReducer,
    products: productsReducer
  },
});
