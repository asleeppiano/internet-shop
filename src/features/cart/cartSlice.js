import { createSlice } from '@reduxjs/toolkit';

function getItemIndex(state, id) {
  return state.items.findIndex(item => item.id === id)
}

function updateLocalStorage(items) {
  localStorage.setItem('cart', JSON.stringify(items))
}

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    items: []
  },
  reducers: {
    setCart: (state, action) => {
      state.items = action.payload
    },
    addItem: (state,action) => {
      state.items.push(action.payload)
    },
    removeItem: (state,action) => {
      const idx = getItemIndex(state, action.payload.id)
      if(idx !== -1) {
        state.items.splice(idx, 1)
        updateLocalStorage(state.items)
      }
    },
    incrementItemCount: (state, action) => {
      const idx = getItemIndex(state, action.payload.id)
      if(idx !== -1) {
        state.items[idx].count += 1
        updateLocalStorage(state.items)
      }
    },
    decrementItemCount: (state, action) => {
      const idx = getItemIndex(state, action.payload.id)
      if(idx !== -1) {
        const count = state.items[idx].count
        if(count === 1) {
          state.items.splice(idx, 1)
        } else {
          state.items[idx].count -= 1
        }
        updateLocalStorage(state.items)
      }
    },
    removeAllItems: (state) => {
      state.items = []
    },
  },
});

export const { setCart, addItem, removeItem, changeItemCount, removeAllItems, incrementItemCount, decrementItemCount } = cartSlice.actions;

export const selectCartSize = state => state.cart.items.reduce((acc, val) => {
  return acc + val.count
}, 0);
export const selectCartItems = state => state.cart.items;
export const selectCartItemById = id => state => state.cart.items.find(item => item.id === id)
export const selectTotalPrice = state => state.cart.items.reduce((acc, val) => {
  return acc + (val.item.price * val.count)
}, 0).toFixed(2)
export default cartSlice.reducer;
