/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectCartItems, selectTotalPrice, removeItem } from './cartSlice.js'

import CartItem from '../../components/CartItem.js'
import Container from '../../components/Container.js'
import ConfirmDialog from '../../components/ConfirmDialog.js'

const ul = css`
  padding: 0;
  margin: 0;
`

const li = css`
  list-style-type: none;
  &:not(:first-of-type) {
    margin-top: 1rem;
  }
`
const total = css`
  font-weight: 700;
  font-size: 1.25rem;
`

const bottom = css`
  max-width: 500px;
  margin: 2rem auto 0 auto;
  text-align: right;
`

export default function Cart() {
  const items = useSelector(selectCartItems);
  const totalPrice = useSelector(selectTotalPrice)
  const dispatch = useDispatch()
  const [showModal, setShowModal] = useState(false)
  const [removeCanditate, setRemoveCandidate] = useState(null)

  function closeModal() {
    setShowModal(false)
  }

  function openModal(item) {
    setRemoveCandidate(item)
    setShowModal(true)
  }

  function remove() {
    dispatch(removeItem(removeCanditate))
    setRemoveCandidate(null)
    setShowModal(false)
  }

  return (
    <Container>
      <h1>Cart</h1>
      <ul css={ul}>
        {items && items.length > 0 ? items.map(item => {
          return (
            <li css={li} key={item.id}>
              <CartItem onRemoveItem={openModal} product={item} />
            </li>
          )
        }) : <li css={li}>Cart is empty. Add products to cart</li>}
      </ul>
      {totalPrice !== '0.00' ?
      <div css={bottom}>
        <span>Total price: </span><span css={total}>${totalPrice}</span>
      </div> : ''}
      <ConfirmDialog text="Do you want to delete this item?" onOk={remove} show={showModal} onClose={closeModal} />
    </Container>
  )
}
