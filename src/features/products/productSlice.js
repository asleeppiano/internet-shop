import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { productAPI } from "../../api";
import { FETCH_STATE } from '../../constants'

export const fetchProductList = createAsyncThunk(
  "products/fetchProductList",
  async () => {
    console.log('hello')
    const response = await productAPI.fetchProductList();
    return response;
  }
);

export const fetchProductListByDealerList = createAsyncThunk(
  "products/fetchProductListByDealerList",
  async (dealerList) => {
    const response = await productAPI.fetchProductListByDealerList(dealerList);
    return response;
  }
);

export const productsSlice = createSlice({
  name: "products",
  initialState: {
    productlist: [],
    fetchState: ''
  },
  extraReducers: {
    [fetchProductList.fulfilled]: (state, action) => {
      const products = action.payload;
      state.productlist = products.map(product => {
        return {item: product, id: product.name}
      });
      state.fetchState = FETCH_STATE.FULFILLED
    },
    [fetchProductList.pending]: (state) => {
      state.fetchState = FETCH_STATE.LOADING
    },
    [fetchProductListByDealerList.fulfilled]: (state, action) => {
      const products = action.payload;
      state.productlist = products.map(product => {
        return {item: product, id: product.name}
      });
      state.fetchState = FETCH_STATE.FULFILLED
    },
    [fetchProductListByDealerList.pending]: (state) => {
      state.fetchState = FETCH_STATE.LOADING
    },
  },
});

export const selectProductList = (state) => state.products.productlist;
export const selectFetchState = (state) => state.products.fetchState;

export default productsSlice.reducer;
