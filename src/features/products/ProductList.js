/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchProductList,
  fetchProductListByDealerList,
  selectProductList,
  selectFetchState,
} from "./productSlice.js";
import { FETCH_STATE } from "../../constants";
import AppContext from "../../context";

import Product from "../../components/Product.js";
import Container from "../../components/Container.js";

const ul = css`
  padding: 0;
  margin: 0;
`;

const li = css`
  list-style-type: none;
  &:not(:first-of-type) {
    margin-top: 1rem;
  }
`;

export default function ProductList() {
  const ctx = useContext(AppContext);
  const productlist = useSelector(selectProductList);
  const fetchState = useSelector(selectFetchState);
  const dispatch = useDispatch();

  useEffect(() => {
    if (ctx && ctx.dealers) {
      dispatch(fetchProductListByDealerList(ctx.dealers.join(",")));
    } else {
      dispatch(fetchProductList());
    }
  }, [dispatch, ctx]);

  return (
    <Container>
      <h1>Product list</h1>
      <ul css={ul}>
        {fetchState === FETCH_STATE.FULFILLED && productlist.length > 0 ? (
          productlist.map((item) => {
            return (
              <li css={li} key={item.id}>
                <Product product={item} />
              </li>
            );
          })
        ) : fetchState === FETCH_STATE.FULFILLED && productlist.length === 0 ? (
          <div>
            no products :(
            <br />
            maybe those dealers do not exist
          </div>
        ) : fetchState === FETCH_STATE.LOADING ? (
          <div>Loading...</div>
        ) : (
          ""
        )}
      </ul>
    </Container>
  );
}
