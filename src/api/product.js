import { API_ADDR } from '../constants'

export async function fetchProductList() {
  console.log('fetch')
  const res = await fetch(`${API_ADDR}/goods`)
  const goods = await res.json()

  if (res.ok) {
    return goods
  } else {
    throw new Error('cannot fetch goods:')
  }
}

export async function fetchProductListByDealerList(dealers) {
  const res = await fetch(`${API_ADDR}/goods?dealers=${dealers}`)
  const goods = await res.json()

  if (res.ok) {
    return goods
  } else {
    throw new Error('cannot fetch goods:')
  }
}

