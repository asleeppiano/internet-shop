import { fetchProductList, fetchProductListByDealerList } from './product.js'
import { fetchDealersIds } from './dealers.js'

export const dealersAPI = {
  fetchDealersIds
}

export const productAPI = {
  fetchProductList,
  fetchProductListByDealerList
}
