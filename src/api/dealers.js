import { API_ADDR } from '../constants'

export async function fetchDealersIds() {
  const res = await fetch(`${API_ADDR}/dealers`)
  const goods = await res.json()

  if (res.ok) {
    return goods
  } else {
    throw new Error('cannot fetch goods:')
  }
}
